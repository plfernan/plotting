import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

from matplotlib.ticker import FormatStrFormatter
from matplotlib.ticker import FuncFormatter

# meas_cpu = pd.read_csv('../data/cpu_meas.csv',index_col='sectors')

meas_1080 = pd.read_csv('../data/1080_meas.csv',index_col='sectors')
meas_2080 = pd.read_csv('../data/2080_meas.csv',index_col='sectors')
meas_v100 = pd.read_csv('../data/v100_meas.csv',index_col='sectors')

cut_1080 = pd.read_csv('../data/1080_compassut_only.csv',index_col='sectors')
cut_2080 = pd.read_csv('../data/2080_compassut_only.csv',index_col='sectors')
cut_v100 = pd.read_csv('../data/v100_compassut_only.csv',index_col='sectors')
cut_cpu_lab15 = pd.read_csv('../data/cpulab15_compassut_only.csv',index_col='sectors')

cern_path = '../output/cern/'
journal_path = '../output/journal/'

# print('##  cpu:')
# print(meas_cpu)
# print('--------------------\n')

print('## Total 1080 Ti:')
print(meas_1080)
print('--------------------\n')

print('## Total 2080 Ti:')
print(meas_2080)
print('--------------------\n')

print('## Total V100:')
print(meas_v100)
print('--------------------\n')

print('## CUT 1080 Ti:')
print(cut_1080)
print('--------------------\n')

print('## CUT 2080 Ti:')
print(cut_2080)
print('--------------------\n')

print('## CUT V100:')
print(cut_v100)
print('--------------------\n')

print('## CUT CPU:')
print(cut_cpu_lab15)
print('--------------------\n')

###################################
# Compare number of sectors, candidates
# for 1 specific GPU
###################################
def compare_num_sectors(dataframe, title, path, filename):

  column_index = list(dataframe.columns.values)

  fig, ax = plt.subplots()

  n_groups = len(column_index)

  #defining an array of colors  
  colors = ['#7A9F35', '#226666', '#AA3939']

  opacity = 1
  bar_width = 0.25
  index = np.arange(n_groups)

  text_x_correction = 0.035
  text_y_correction = 3000

  ax.bar(index - bar_width, 
        dataframe.loc[ 1 , : ], 
        bar_width, 
        alpha=opacity,
        color=colors[0],
        label='1 sector')

  # Put labels inside each bar
  for i in range(n_groups):
    plt.text(x = index[i] - bar_width - text_x_correction, 
            y = dataframe.loc[ 1 , : ][i] - text_y_correction, 
            s = dataframe.loc[ 1 , : ][i], 
            rotation=90, 
            size=6)

  ax.bar(index, 
        dataframe.loc[ 3 , : ], 
        bar_width, 
        alpha=opacity,
        color=colors[1],
        label='3 sectors')

  # Put labels inside each bar
  for i in range(n_groups):
    plt.text(x = index[i] - text_x_correction, 
            y = dataframe.loc[ 3 , : ][i] - text_y_correction, 
            s = dataframe.loc[ 3 , : ][i], 
            rotation=90, 
            size=6)

  ax.bar(index + bar_width, 
        dataframe.loc[ 5 , : ], 
        bar_width, 
        alpha=opacity,
        color=colors[2],
        label='5 sectors')

  # Put labels inside each bar
  for i in range(n_groups):
    plt.text(x = index[i] + bar_width - text_x_correction, 
            y = dataframe.loc[ 5 , : ][i] - text_y_correction, 
            s = dataframe.loc[ 5 , : ][i], 
            rotation=90, 
            size=6)  

  ax.set_xlabel('Candidates')
  ax.set_ylabel('Throughput (events/s)')
  ax.set_title(title)
  ax.set_xticks(index + bar_width / 3)
  ax.set_xticklabels(column_index)
  # ax.legend()
  ax.legend(loc='lower left', bbox_to_anchor= (1.0, 0.8), ncol=1, 
            borderaxespad=0, frameon=False)

  #removing top and right borders
  ax.spines['top'].set_visible(False)
  ax.spines['right'].set_visible(False)

  #adds major gridlines
  ax.grid(color='grey', linestyle='dashed', linewidth=0.25, alpha=0.5)
  ax.set_axisbelow(True)

  # print(index)
  # print(column_index)

  fig.tight_layout()
  # plt.show()
  plt.savefig(path + filename + '.pdf', format='pdf')


###################################
# Compare different gpus
###################################

def compare_gpus(df_1080, name_1080, df_2080, name_2080, df_v100, name_v100, sector, title, path, filename):
  # The other df should be the same
  column_index = list(df_1080.columns.values)
  n_groups = len(column_index)
  index = np.arange(n_groups)

  # define custom array of colors
  colors = ['#7A9F35', '#226666', '#AA3939']

  fig, ax = plt.subplots()

  opacity = 1
  bar_width = 0.2

  text_x_correction = 0.035
  text_y_correction = 4000

  # 1080 Ti bar plot
  ax.bar(index - bar_width, 
        df_1080.loc[ sector , : ], 
        bar_width, 
        alpha=opacity,
        color=colors[0],
        label=name_1080)

  # Put labels inside each bar
  for i in range(n_groups):
    plt.text(x = index[i] - bar_width - text_x_correction, 
            y = df_1080.loc[ sector , : ][i] - text_y_correction, 
            s = df_1080.loc[ sector , : ][i], 
            rotation=90, 
            size=6)

  # 2080 Ti bar plot
  ax.bar(index, 
        df_2080.loc[ sector , : ], 
        bar_width, 
        alpha=opacity,
        color=colors[1],
        label=name_2080)

  # Put labels inside each bar
  for i in range(n_groups):
    plt.text(x = index[i] - text_x_correction, 
            y = df_2080.loc[ sector , : ][i] - text_y_correction, 
            s = df_2080.loc[ sector , : ][i], 
            rotation=90, 
            size=6)

  # V100 bar plot
  ax.bar(index + bar_width, 
        df_v100.loc[ sector , : ], 
        bar_width, 
        alpha=opacity,
        color=colors[2],
        label=name_v100)

  # Put labels inside each bar
  for i in range(n_groups):
    plt.text(x = index[i] + bar_width - text_x_correction, 
            y = df_v100.loc[ sector , : ][i] - text_y_correction, 
            s = df_v100.loc[ sector , : ][i], 
            rotation=90, 
            size=6)

  ax.set_xlabel('Candidates')
  ax.set_ylabel('Throughput (events/s)')
  ax.set_title(title)
  ax.set_xticks(index + bar_width / 4)
  ax.set_xticklabels(column_index)
  ax.legend(loc='lower left', bbox_to_anchor= (1.0, 0.8), ncol=1, 
            borderaxespad=0, frameon=False)

  #removing top and right borders
  ax.spines['top'].set_visible(False)
  ax.spines['right'].set_visible(False)

  #adds major gridlines
  ax.grid(color='grey', linestyle='dashed', linewidth=0.25, alpha=0.5)
  ax.set_axisbelow(True)

  # plt.ylim(40000, 1200000)

  fig.tight_layout()
  # plt.show()
  plt.savefig(path + filename + '.pdf', format='pdf')


def price_performance_ratio(df_1080, name_1080, df_2080, name_2080, df_v100, name_v100, sector, title, path, filename):

  columns = [name_1080, name_2080, name_v100]
  n_groups = len(columns)
  index = np.arange(n_groups)

  #defining an array of colors  
  colors = ['#7A9F35', '#226666', '#AA3939']

  fig, ax = plt.subplots()

  # Price in $ 
  msrp_1080 = 699
  msrp_2080 = 1200
  msrp_v100 = 7374

  # goal_events_per_sec = 30000000
  goal_events_per_sec = 30000

  # Plot for 16 candidates case
  value_1080 = goal_events_per_sec / df_1080.loc[ sector , '16cand' ] * msrp_1080
  value_2080 = goal_events_per_sec / df_2080.loc[ sector , '16cand' ] * msrp_2080
  value_v100 = goal_events_per_sec / df_v100.loc[ sector , '16cand' ] * msrp_v100

  final_values = [round(value_1080,2), round(value_2080,2), round(value_v100,2)]

  ax.bar(columns, final_values)

  # Put labels on top of each bar
  for i in range(n_groups):
    # plt.text(x = index[i]-0.5 , y = final_values[i]+0.1, s = final_values[i], size = 6)
    plt.text(x = index[i] - 0.15, y = final_values[i] + goal_events_per_sec/250, s = final_values[i])

  ax.set_xlabel('GPUs')
  ax.set_ylabel('$ for 30k events/s')
  ax.set_title(title)
  # ax.set_xticks(index)
  # ax.set_xticklabels(columns)
  # ax.legend(loc='lower left', bbox_to_anchor= (1.0, 0.8), ncol=1, 
  #           borderaxespad=0, frameon=False)

  #removing top and right borders
  ax.spines['top'].set_visible(False)
  ax.spines['right'].set_visible(False)

  #adds major gridlines
  ax.grid(color='grey', linestyle='dashed', linewidth=0.25, alpha=0.5)
  ax.set_axisbelow(True)

  # plt.ylim(40000, 1200000)

  fig.tight_layout()
  # plt.show()
  plt.savefig(path + filename + '.pdf', format='pdf') 



###################################
# Compare different gpus with cpu
###################################
def compare_gpus_cpu(df_1080, name_1080, df_2080, name_2080, df_v100, name_v100, df_cpu, name_cpu, sector, title, path, filename):
  # The other df should be the same
  column_index = list(df_1080.columns.values)
  n_groups = len(column_index)
  index = np.arange(n_groups)

  # define custom array of colors
  colors = ['#7A9F35', '#226666', '#AA3939', '#FFB4A2']

  fig, ax = plt.subplots()

  opacity = 1
  bar_width = 0.2

  text_x_correction = 0.035
  text_y_correction = 24000

  # 1080 Ti bar plot
  ax.bar(index - bar_width, 
        df_1080.loc[ sector , : ], 
        bar_width, 
        alpha=opacity,
        color=colors[0],
        label=name_1080)

  # Put labels inside each bar
  for i in range(n_groups):
    plt.text(x = index[i] - bar_width - text_x_correction, 
            y = df_1080.loc[ sector , : ][i] - text_y_correction, 
            s = df_1080.loc[ sector , : ][i], 
            rotation=90, 
            size=6)

  # 2080 Ti bar plot
  ax.bar(index, 
        df_2080.loc[ sector , : ], 
        bar_width, 
        alpha=opacity,
        color=colors[1],
        label=name_2080)

  # Put labels inside each bar
  for i in range(n_groups):
    plt.text(x = index[i] - text_x_correction, 
            y = df_2080.loc[ sector , : ][i] - text_y_correction, 
            s = df_2080.loc[ sector , : ][i], 
            rotation=90, 
            size=6)

  # V100 bar plot
  ax.bar(index + bar_width, 
        df_v100.loc[ sector , : ], 
        bar_width, 
        alpha=opacity,
        color=colors[2],
        label=name_v100)

  # Put labels inside each bar
  for i in range(n_groups):
    plt.text(x = index[i] + bar_width - text_x_correction, 
            y = df_v100.loc[ sector , : ][i] - text_y_correction, 
            s = df_v100.loc[ sector , : ][i], 
            rotation=90, 
            size=6)

  # CPU bar plot
  ax.bar(index + bar_width + bar_width, 
        df_cpu.loc[ sector , : ], 
        bar_width, 
        alpha=opacity,
        color=colors[3],
        label=name_cpu)

  # Put labels inside each bar
  for i in range(n_groups):
    plt.text(x = index[i] + bar_width + bar_width - text_x_correction, 
            y = df_cpu.loc[ sector , : ][i] - text_y_correction, 
            s = df_cpu.loc[ sector , : ][i], 
            rotation=90, 
            size=6)

  ax.set_xlabel('Candidates')
  ax.set_ylabel('Throughput (events/s)')
  ax.set_title(title)
  ax.set_xticks(index + bar_width / 4)
  ax.set_xticklabels(column_index)
  ax.legend(loc='lower left', bbox_to_anchor= (1.0, 0.8), ncol=1, 
            borderaxespad=0, frameon=False)

  #removing top and right borders
  ax.spines['top'].set_visible(False)
  ax.spines['right'].set_visible(False)

  #adds major gridlines
  ax.grid(color='grey', linestyle='dashed', linewidth=0.25, alpha=0.5)
  ax.set_axisbelow(True)

  # plt.ylim(40000, 1200000)

  fig.tight_layout()
  # plt.show()
  plt.savefig(path + filename + '.pdf', format='pdf')


###################################
# JOURNAL - Make combined plot
###################################
def throughput_side_by_side_cpu(df_cpu, name_cpu, df_1080, name_1080, df_2080, name_2080, df_v100, name_v100, title, path, filename):

  fig, (sect3, sect5) = plt.subplots(1, 2, figsize=(10,5))

  ##### common stuff

  fig.suptitle(title, size=16)

  column_index = list(df_cpu.columns.values)
  n_groups = len(column_index)
  index = np.arange(n_groups)

  #defining an array of colors  
  colors = ['#7A9F35', '#226666', '#AA3939', '#FFB4A2']

  opacity = 1
  bar_width = 0.2

  # Create a formatter to set the ticks from 400000 to 400 k
  to_k_lambda = lambda x, pos: '%d k' % (x * 1e-3)
  formatter = FuncFormatter(to_k_lambda)

  ##### 3 sectors plot

  # CPU bar plot
  ax_cpu = sect3.bar(index - (bar_width / 2) - bar_width, 
        df_cpu.loc[ 3 , : ], 
        bar_width, 
        alpha=opacity,
        color=colors[0],
        label=name_cpu)

  # 1080 Ti bar plot
  ax_1080 = sect3.bar(index - (bar_width / 2), 
        df_1080.loc[ 3 , : ], 
        bar_width, 
        alpha=opacity,
        color=colors[1],
        label=name_1080)

  # 2080 Ti bar plot
  ax_2080 = sect3.bar(index + (bar_width / 2), 
        df_2080.loc[ 3 , : ], 
        bar_width, 
        alpha=opacity,
        color=colors[2],
        label=name_2080)

  # V100 bar plot
  ax_v100 = sect3.bar(index + (bar_width / 2) + bar_width, 
        df_v100.loc[ 3 , : ], 
        bar_width, 
        alpha=opacity,
        color=colors[3],
        label=name_v100)

  sect3.set_xlabel('Candidates')
  sect3.set_ylabel('Throughput (events/s)')
  sect3.set_title('3 sectors')
  # sect3.set_xticks(index + bar_width / 4)
  sect3.set_xticks(index)
  sect3.set_xticklabels(column_index)

  #removing top and right borders
  sect3.spines['top'].set_visible(False)
  sect3.spines['right'].set_visible(False)

  #adds major gridlines
  sect3.grid(color='grey', linestyle='dashed', linewidth=0.25, alpha=0.5)
  sect3.set_axisbelow(True)

  # force the y axis so both look the same
  sect3.set_ylim([0, 830000])
  
  sect3.yaxis.set_major_formatter(formatter)

  ##### 5 sectors plot

  # CPU bar plot
  sect5.bar(index - (bar_width / 2) - bar_width, 
        df_cpu.loc[ 5 , : ], 
        bar_width, 
        alpha=opacity,
        color=colors[0],
        label=name_cpu)

  # 1080 Ti bar plot
  sect5.bar(index - (bar_width / 2), 
        df_1080.loc[ 5 , : ], 
        bar_width, 
        alpha=opacity,
        color=colors[1],
        label=name_1080)

  # 2080 Ti bar plot
  sect5.bar(index + (bar_width / 2), 
        df_2080.loc[ 5 , : ], 
        bar_width, 
        alpha=opacity,
        color=colors[2],
        label=name_2080)

  # V100 bar plot
  sect5.bar(index + (bar_width / 2) + bar_width, 
        df_v100.loc[ 5 , : ], 
        bar_width, 
        alpha=opacity,
        color=colors[3],
        label=name_v100)

  sect5.set_xlabel('Candidates')
  sect5.set_ylabel('Throughput (events/s)')
  sect5.set_title('5 sectors')
  sect5.set_xticks(index)
  sect5.set_xticklabels(column_index)

  #removing top and right borders
  sect5.spines['top'].set_visible(False)
  sect5.spines['right'].set_visible(False)

  #adds major gridlines
  sect5.grid(color='grey', linestyle='dashed', linewidth=0.25, alpha=0.5)
  sect5.set_axisbelow(True)

  # force the y axis so both look the same
  sect5.set_ylim([0, 830000])

  sect5.yaxis.set_major_formatter(formatter)

  ##### common stuff

  my_lgd = fig.legend(
    (ax_cpu, ax_1080, ax_2080, ax_v100), 
    (name_cpu, name_1080, name_2080, name_v100),
    loc="center",
    borderaxespad=0.1,
    bbox_to_anchor=(0.5, 0.05),
    ncol=2)

  fig.tight_layout()

  # Adjust the scaling factor to fit your legend text completely outside the plot
  # (smaller value results in more space being made for the legend)
  fig.subplots_adjust(bottom=0.2)

  

  # plt.show()
  plt.savefig(path + filename + '.pdf', format='pdf')


###################################
# JOURNAL - Price performance 
# with CPU
###################################
def price_performance_ratio(df_cpu, name_cpu, df_1080, name_1080, df_2080, name_2080, df_v100, name_v100, title, path, filename):

  columns = [name_cpu, name_1080, name_2080, name_v100]
  n_groups = len(columns)
  index = np.arange(n_groups)

  #defining an array of colors  
  colors = ['#7A9F35', '#226666', '#AA3939', '#FFB4A2']

  fig, ax = plt.subplots()

  # Price in $ 
  msrp_cpu = 2 * 2141
  msrp_1080 = 700
  msrp_2080 = 1200
  msrp_v100 = 10664

  # Create a formatter to add $
  to_k_lambda = lambda x, pos: '%d $' % (x)
  formatter = FuncFormatter(to_k_lambda)

  # TODO single-unit list price before any applicable discounts (ex: EDU, volume)

  # goal_events_per_sec = 30000000
  goal_events_per_sec = 30000

  # Plot for 3 sectors - 16 candidates case
  value_cpu_3  = goal_events_per_sec / df_cpu.loc [ 3 , '16' ] * msrp_cpu
  value_1080_3 = goal_events_per_sec / df_1080.loc[ 3 , '16' ] * msrp_1080
  value_2080_3 = goal_events_per_sec / df_2080.loc[ 3 , '16' ] * msrp_2080
  value_v100_3 = goal_events_per_sec / df_v100.loc[ 3 , '16' ] * msrp_v100

  # Plot for 5 sectors - 16 candidates case
  value_cpu_5  = goal_events_per_sec / df_cpu.loc [ 5 , '16' ] * msrp_cpu
  value_1080_5 = goal_events_per_sec / df_1080.loc[ 5 , '16' ] * msrp_1080
  value_2080_5 = goal_events_per_sec / df_2080.loc[ 5 , '16' ] * msrp_2080
  value_v100_5 = goal_events_per_sec / df_v100.loc[ 5 , '16' ] * msrp_v100

  final_values_3 = [round(value_cpu_3,2), round(value_1080_3,2), round(value_2080_3,2), round(value_v100_3,2)]
  final_values_5 = [round(value_cpu_5,2), round(value_1080_5,2), round(value_2080_5,2), round(value_v100_5,2)]

  bar_width = 0.4

  sect3 = ax.bar(index - (bar_width / 2), 
    final_values_3,
    bar_width,
    color=colors[0],
    label='3 sectors')
  sect5 = ax.bar(index + (bar_width / 2), 
    final_values_5,
    bar_width,
    color=colors[2],
    label='5 sectors')

  # # Put labels on top of each bar
  # for i in range(n_groups):
  #   plt.text(
  #     x = index[i] - bar_width, 
  #     y = final_values_3[i] + goal_events_per_sec/150, 
  #     s = final_values_3[i],
  #     rotation=45)

  # # Put labels on top of each bar
  # for i in range(n_groups):
  #   plt.text(
  #     x = index[i], 
  #     y = final_values_5[i] + goal_events_per_sec/150, 
  #     s = final_values_5[i],
  #     rotation=45)

  # ax.set_ylim([0, 1600])
  ax.yaxis.set_major_formatter(formatter)

  ax.set_xlabel('Unit')
  ax.set_ylabel('Price for 30k events/s')
  ax.set_title(title)
  ax.set_xticks(index)
  ax.set_xticklabels(columns)
  ax.legend(
    loc='upper right', 
    # bbox_to_anchor= (0.5, 0.55), 
    ncol=1, 
    borderaxespad=0.1)

  #removing top and right borders
  ax.spines['top'].set_visible(False)
  ax.spines['right'].set_visible(False)

  #adds major gridlines
  ax.grid(color='grey', linestyle='dashed', linewidth=0.25, alpha=0.5)
  ax.set_axisbelow(True)

  fig.tight_layout()
  # plt.show()
  plt.savefig(path + filename + '.pdf', format='pdf') 

###################################
# Kernels time
###################################

def kernels_time(path, filename):
  algs_cut = ['ut_search_windows', 'compass_ut']
  time_cut = [29.25, 18.55]

  algs_dec = ['ut_decode_raw_banks_in_order', 'prefix_sum', 'ut_pre_decode', 'ut_find_permutation', 'ut_calculate_number_of_hits']
  time_dec = [14.07, 14.0, 11.17, 7.48, 5.48]

  fig, ax = plt.subplots()

  colors = ['#7A9F35', '#226666', '#AA3939', '#FFB4A2']

  ax.xaxis.grid(True, color='grey', linestyle="dashed", linewidth=0.25, alpha=0.5)
  ax.set_axisbelow(True)

  ax.barh(algs_cut, time_cut, align='center', color=colors[0], label='CompassUT (47.8 %)')
  ax.barh(algs_dec, time_dec, align='center', color=colors[2], label='UT decoding (52.2 %)')

  plt.legend()

  ax.invert_yaxis()  # labels read top-to-bottom

  fig.tight_layout()

  plt.savefig(path + filename + '.pdf', format='pdf') 

###################################
# Plot stuff
###################################

# # compare_num_candidates(meas_cpu, 'CPU - Number of sectors', cern_path, 'number_of_sectors_cpu')
# compare_num_sectors(meas_1080, '1080 Ti - Number of sectors', cern_path, 'number_of_sectors_1080')
# compare_num_sectors(meas_2080, '2080 Ti - Number of sectors', cern_path, 'number_of_sectors_2080')
# compare_num_sectors(meas_v100, 'V100 - Number of sectors', cern_path, 'number_of_sectors_v100')

# compare_gpus(meas_1080, '1080 Ti', meas_2080, '2080 Ti', meas_v100, 'V100', 1, 'GPU comparison - 1 sector', cern_path, 'gpu_comparison_1')
# compare_gpus(meas_1080, '1080 Ti', meas_2080, '2080 Ti', meas_v100, 'V100', 3, 'GPU comparison - 3 sectors', cern_path, 'gpu_comparison_3')
# compare_gpus(meas_1080, '1080 Ti', meas_2080, '2080 Ti', meas_v100, 'V100', 5, 'GPU comparison - 5 sectors', cern_path, 'gpu_comparison_5')

# compare_gpus_cpu(cut_1080, '1080 Ti', cut_2080, '2080 Ti', cut_v100, 'V100', cut_cpu_lab15, '2x Xeon', 1, 'CPU/GPU comparison - 1 sector', cern_path, 'cut_cpu_gpu_comparison_1')
# compare_gpus_cpu(cut_1080, '1080 Ti', cut_2080, '2080 Ti', cut_v100, 'V100', cut_cpu_lab15, '2x Xeon', 3, 'CPU/GPU comparison - 3 sector', cern_path, 'cut_cpu_gpu_comparison_3')
# compare_gpus_cpu(cut_1080, '1080 Ti', cut_2080, '2080 Ti', cut_v100, 'V100', cut_cpu_lab15, '2x Xeon', 5, 'CPU/GPU comparison - 5 sector', cern_path, 'cut_cpu_gpu_comparison_5')

# compare_num_sectors(cut_1080, '1080 Ti- Number of sectors', cern_path, 'cut_number_of_sectors_1080')
# compare_num_sectors(cut_2080, '2080 Ti- Number of sectors', cern_path, 'cut_number_of_sectors_2080')
# compare_num_sectors(cut_v100, 'V100 - Number of sectors', cern_path, 'cut_number_of_sectors_v100')
# compare_num_sectors(cut_cpu_lab15, '2x Xeon - Number of sectors', cern_path, 'cut_number_of_sectors_cpu')

# price_performance_ratio(meas_1080, '1080 Ti', meas_2080, '2080 Ti', meas_v100, 'V100', 3, 'Price/performance\n$ for 30k events/s\n3 sectors - 16 candidates', cern_path, 'price_perf_3_sectors')
# price_performance_ratio(meas_1080, '1080 Ti', meas_2080, '2080 Ti', meas_v100, 'V100', 5, 'Price/performance\n$ for 30k events/s\n5 sectors - 16 candidates', cern_path, 'price_perf_5_sectors')

###################################
# Paper plots
###################################

throughput_side_by_side_cpu(
  cut_cpu_lab15, '2 x Intel(R) Xeon(R) CPU E5-2687W v3', 
  cut_1080, 'GeForce GTX 1080 Ti', 
  cut_2080, 'GeForce RTX 2080 Ti', 
  cut_v100, 'Tesla V100', 
  '3 vs 5 sectors comparison', journal_path, '3vs5sectors_comparison')

price_performance_ratio(
  cut_cpu_lab15, '2 x Intel(R) Xeon(R) \nCPU E5-2687W v3', 
  cut_1080, 'GeForce \nGTX 1080 Ti', 
  cut_2080, 'GeForce \nRTX 2080 Ti', 
  cut_v100, 'Tesla V100', 
  'Price/performance for 30k events/s', journal_path, 'price_performance_3_5')

kernels_time(journal_path, 'kernels_time')