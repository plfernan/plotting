import pandas as pd

import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as plt

import matplotlib.pylab as pylab

import numpy as np

pylab.rcParams['figure.figsize'] = 10, 10 # figure size
# fsize = 44 # font size
# leg_fsize = 36 # legend
# mark_size = 18 # marker
# offset_size = 30 # scientific exponent size

mpl.rcParams['legend.fontsize'] = 10

fig = plt.figure()
ax = fig.gca(projection='3d')

df = pd.read_csv('/home/plfernan/cernbox/workspace/kalman_weight_matrix/out/expected_track_coords_fwd_upd_dp_0.csv')
df1 = pd.read_csv('/home/plfernan/cernbox/workspace/kalman_weight_matrix/out/wfit_track_coords_fwd_upd_dp_0.csv')

# Plot Z plane as X coord...
ax.scatter(df['z'], df['x'], df['y'], label='expected track points')
ax.plot(df['z'], df['x'], df['y'], label='expected track')

ax.scatter(df['z'], df1['x'], df1['y'], label='expected track points')
ax.plot(df['z'], df1['x'], df1['y'], label='expected track')

# ax.scatter(df['z'], df1['x'], df1['y'], label='wfit track points')
# ax.plot(df['z'], df1['x'], df1['y'], label='wfit track')

# point1  = np.array([7982.47, 700,1000])
# normal1 = np.array([0,0,1])

# point2  = np.array([9229.48, 700,1000])
# normal2 = np.array([0,0,1])

# point3  = np.array([125.93, 700,1000])
# normal3 = np.array([0,0,1])
# d1 = -np.sum(point1*normal1)# dot product
# d2 = -np.sum(point2*normal2)# dot product
# d3 = -np.sum(point3*normal3)# dot product

# xx, yy = np.meshgrid(range(1000), range(1000))

# z1 = (-normal1[0]*xx - normal1[1]*yy - d1)*1./normal1[2]
# z2 = (-normal2[0]*xx - normal2[1]*yy - d2)*1./normal2[2]
# z3 = (-normal3[0]*xx - normal3[1]*yy - d3)*1./normal3[2]

# ax.plot_surface(z1, xx,yy, color='blue', alpha=0.2)
# ax.plot_surface(z2, xx,yy, color='blue', alpha=0.2)
# ax.plot_surface(z3, xx,yy, color='blue', alpha=0.2)

ax.set_xlabel('$Z$', fontsize=20)
ax.set_ylabel('$X$', fontsize=20)
ax.set_zlabel('$Y$', fontsize=20)

ax.legend()
ax.view_init(azim=80)

plt.tight_layout()

plt.show()