import glob
import os

import pandas as pd

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from matplotlib.ticker import ScalarFormatter
import matplotlib.pylab as pylab
import seaborn as sns

import numpy as np

# Set the plot parameters

## Big figure
pylab.rcParams['figure.figsize'] = 24, 20 # figure size
fsize = 44 # font size
leg_fsize = 36 # legend
mark_size = 18 # marker
offset_size = 30 # scientific exponent size

## Normal figure
# pylab.rcParams['figure.figsize'] = 10, 7
# fsize = 22
# leg_fsize = 16
# mark_size = 9
# offset_size = 15

# globals
INDEX_COL = 'node'
Z_COL = 'z'

# paths
DATA_PATH = '../data/'
OUTPUT_PATH = '../output/'

# Read all the tracks
print('Reading all tracks...')
list_we_err_fwd_pred_dp = glob.glob(DATA_PATH + 'we_track_err_fwd_pred_dp_*.csv')
list_we_err_fwd_upd_dp = glob.glob(DATA_PATH + 'we_track_err_fwd_upd_dp_*.csv')
list_we_err_fwd_pred_sp = glob.glob(DATA_PATH + 'we_track_err_fwd_pred_sp_*.csv')
list_we_err_fwd_upd_sp = glob.glob(DATA_PATH + 'we_track_err_fwd_upd_sp_*.csv')

list_we_err_bwd_pred_dp = glob.glob(DATA_PATH + 'we_track_err_bwd_pred_dp_*.csv')
list_we_err_bwd_upd_dp = glob.glob(DATA_PATH + 'we_track_err_bwd_upd_dp_*.csv')
list_we_err_bwd_pred_sp = glob.glob(DATA_PATH + 'we_track_err_bwd_pred_sp_*.csv')
list_we_err_bwd_upd_sp = glob.glob(DATA_PATH + 'we_track_err_bwd_upd_sp_*.csv')

list_err_fwd_pred_dp = glob.glob(DATA_PATH + 'track_err_fwd_pred_dp_*.csv')
list_err_fwd_upd_dp = glob.glob(DATA_PATH + 'track_err_fwd_upd_dp_*.csv')
list_err_fwd_pred_sp = glob.glob(DATA_PATH + 'track_err_fwd_pred_sp_*.csv')
list_err_fwd_upd_sp = glob.glob(DATA_PATH + 'track_err_fwd_upd_sp_*.csv')
print('Done!\n')

# Read just the first track
# we_err_pred_dp = pd.read_csv('../../data/wmatrix/wetrack_err_fwd_pred_dp_0.csv', index_col=INDEX_COL)
# we_err_upd_dp = pd.read_csv('../../data/wmatrix/wetrack_err_fwd_upd_dp_0.csv', index_col=INDEX_COL)
# we_err_pred_sp = pd.read_csv('../../data/wmatrix/wetrack_err_fwd_pred_sp_0.csv', index_col=INDEX_COL)
# we_err_upd_sp = pd.read_csv('../../data/wmatrix/wetrack_err_fwd_upd_sp_0.csv', index_col=INDEX_COL)

####################################################################

# Save a plot in SVG and PDF format in its own folder
def saveFig(myplt, filename, tight=True):
  if (tight):    
    myplt.savefig(OUTPUT_PATH + filename + '.svg', bbox_inches='tight')
  else:
    myplt.savefig(OUTPUT_PATH + filename + '.svg')
    # convert to PDF with system Inkscape (installed)
    os.system('inkscape ' + OUTPUT_PATH + filename + '.svg --export-pdf=' + OUTPUT_PATH + 'pdf/' + filename + ".pdf")
    # clear figures
    # myplt.show()
    myplt.gcf().clear()

# Plot a dataframe of (x,y,tx,ty,qp) states
def plot_error(data, saveName, title):
  node = list(data.index.values)
  bar_width = 20

  plt.title(title, fontsize=fsize)

  plt.bar(node, data['x'], label="x", width=bar_width)
  plt.bar(node, data['y'], label="y", width=bar_width)
  # plt.bar(node, data['t_x'], label="t_x", width=bar_width)
  # plt.bar(node, data['t_y'], label="t_y", width=bar_width)
  # plt.bar(node, data['q/pc'], label="q/pc", width=bar_width)

  plt.xlabel("Z plane", fontsize=fsize)
  plt.ylabel("Error", fontsize=fsize)

  plt.yscale("log", basey=10)

  # fig, ax = plt.subplots()
  # # ax.set_yticks((1,5,10,100,1000))
  # # ax.get_yaxis().set_major_formatter(ScalarFormatter())
  # ax.yaxis.set_major_formatter(ScalarFormatter())

  plt.xticks(fontsize=fsize)
  plt.yticks(fontsize=fsize)
  plt.grid()
  plt.legend(loc=2, fontsize=leg_fsize)

  saveFig(plt, saveName, False)

####################################################################

# Return a dataframe of all the zplanes accumulated
# The mean values is still not calculate (use count for that)
def accumulate_tracks(list_files):

  # Create placeholder dataframe for all zplanes (to hold the mean values)
  zplanes = pd.DataFrame(columns=['x', 'y', 't_x', 't_y', 'q/pc', 'count'])
  zplanes.index.name = 'z'

  # Add the values
  for file in list_files:
    err = pd.read_csv(file, index_col=Z_COL)
      # print('File is: {}'.format(file))
      # print(err)
    for index,row in err.iterrows():
      if row.name in zplanes.index:
        # add value and increment count
        x_aux = zplanes.loc[row.name, 'x'] + row['x']
        y_aux = zplanes.loc[row.name, 'y'] + row['y']
        tx_aux = zplanes.loc[row.name, 't_x'] + row['t_x']
        ty_aux = zplanes.loc[row.name, 't_y'] + row['t_y']
        qp_aux = zplanes.loc[row.name, 'q/pc'] + row['q/pc']
        count = zplanes.loc[row.name, 'count'] + 1

        zplanes.at[row.name, 'x'] = x_aux
        zplanes.at[row.name, 'y'] = y_aux
        zplanes.at[row.name, 't_x'] = tx_aux
        zplanes.at[row.name, 't_y'] = ty_aux
        zplanes.at[row.name, 'q/pc'] = qp_aux
        zplanes.at[row.name, 'count'] = count
      else:
        # add new row with new z value
        item = pd.Series({
          'x': row['x'], \
          'y': row['y'], \
          't_x': row['t_x'], \
          't_y': row['t_y'], \
          'q/pc': row['q/pc'], \
          'count': 1 \
        })
        item.name = row.name

        zplanes = zplanes.append(item)

  # Get mean values for each occurrence
  for index,row in zplanes.iterrows():
    row.at['x'] = row.at['x'] / row.at['count']
    row.at['y'] = row.at['y'] / row.at['count']
    row.at['t_x'] = row.at['t_x'] / row.at['count']
    row.at['t_y'] = row.at['t_y'] / row.at['count']
    row.at['q/pc'] = row.at['q/pc'] / row.at['count']

  return zplanes

####################################################################

def showStatistics(df):
  print(df)

####################################################################

print('Generating plots...')

mean_z_we_fwd_pred_dp = accumulate_tracks(list_we_err_fwd_pred_dp)
plot_error(mean_z_we_fwd_pred_dp, "we_err_pred_dp", "Predict - DP")
showStatistics(mean_z_we_fwd_pred_dp)
mean_z_we_fwd_upd_dp = accumulate_tracks(list_we_err_fwd_upd_dp)
plot_error(mean_z_we_fwd_upd_dp, "we_err_upd_dp", "Update - DP")

mean_z_fwd_pred_dp = accumulate_tracks(list_err_fwd_pred_dp)
plot_error(mean_z_fwd_pred_dp, "err_pred_dp", "Predict - DP")
showStatistics(mean_z_fwd_pred_dp)
mean_z_fwd_upd_dp = accumulate_tracks(list_err_fwd_upd_dp)
plot_error(mean_z_fwd_upd_dp, "err_upd_dp", "Update - DP")

print('Done! - Generating plots')

# plot_error(we_err_pred_dp, "we_err_pred_dp", "Predict - DP")
# plot_error(we_err_upd_dp, "we_err_upd_dp", "Update - DP")
# plot_error(we_err_pred_sp, "we_err_pred_sp", "Predict - SP")
# plot_error(we_err_upd_sp, "we_err_upd_sp", "Update - SP")