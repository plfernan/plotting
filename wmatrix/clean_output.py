import os
import glob

files_svg = glob.glob('./output/*.svg')
files_pdf = glob.glob('./output/pdf/*.pdf')

svg_count = 0
for f in files_svg:
  os.remove(f)
  svg_count = svg_count + 1

pdf_count = 0
for f in files_pdf:
  os.remove(f)
  pdf_count = pdf_count + 1

print('Removed {} svg files.'.format(svg_count))
print('Removed {} pdf files.'.format(pdf_count))